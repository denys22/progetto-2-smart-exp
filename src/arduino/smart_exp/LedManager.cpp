#include "LedManager.h"

LedManager::LedManager(int pinLed1, int pinLed2, int blinkPeriod) : TimeTriggeredTask(blinkPeriod){
    this->energyStateLed = new Led(pinLed1);
    this->experimentStateLed = new Led(pinLed2);
    lastState = FINISH_EXPERIMENT;
}

LedManager::~LedManager(){
    delete energyStateLed;
    delete experimentStateLed;
}

void LedManager::init(){
    energyStateLed->initDevice();
    experimentStateLed->initDevice();
}

void LedManager::tick(){
    if(state != lastState){
        lastState = state;
        switch(state){
            case START:
                energyStateLed->switchOn();
                experimentStateLed->switchOff();
                isExperimentStateLedOn = false;
                break;
            case EXPERIMENT:
                experimentStateLed->switchOn();
                isExperimentStateLedOn = true;
                break;
            case POWER_SAVE:
                energyStateLed->switchOff();
                break;
        }
    }
    if(state == ABORT_EXPERIMENT || state == FINISH_EXPERIMENT){
        doBlink();
    }
}

bool LedManager::isReady(int schedulerPeriod){
    return (updateAndCheckTime(schedulerPeriod) || state != lastState);
}

void LedManager::doBlink(){
    isExperimentStateLedOn ?  experimentStateLed->switchOff() : experimentStateLed->switchOn();
    isExperimentStateLedOn = !isExperimentStateLedOn;
}
