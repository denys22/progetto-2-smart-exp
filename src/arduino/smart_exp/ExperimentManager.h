#ifndef __EXPERIMENTMANAGER__
#define __EXPERIMENTMANAGER__

#include "Task.h"
#include "Potentiometer.h"
#include "Sonar.h"
#include "ServoTimer2.h"
#include "Calculator.h"
#include "Globals.h"

#define MAX_TIME 20000
#define ERROR_TIME 2000

class ExperimentManager: public TimeTriggeredTask{
public:
    ExperimentManager(int pinPot, int pinTem, int pinSonEcho, int pinSonTrig, int pinSer, int period);
    ~ExperimentManager();

    void init();
    void tick();
    bool isReady(int period);
      
private:
    Potentiometer* pot;
    Sonar* son;
    ServoTimer2* ser;
    Calculator* cal;
    unsigned long timeStart;
    unsigned timeSonar;
};

#endif
