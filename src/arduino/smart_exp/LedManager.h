#ifndef __LEDMANAGER__
#define __LEDMANAGER__

#include "Led.h"
#include "Task.h"
#include "Globals.h"

class LedManager : public TimeTriggeredTask{
public:
    LedManager(int pinLed1, int pinLed2, int blinkPeriod);
    ~LedManager();

    void init();
    void tick();
    bool isReady(int schedulerPeriod);

private:
    Led* energyStateLed; // The led than rapresent energy state of system
    Led* experimentStateLed; // The led than rapresent current state of experiment
    bool isExperimentStateLedOn; // nedeed for blinking

    void doBlink(); // if the experimentStateLed is turn on, make it turn off. Otherwise make it turn on
};

#endif
