#ifndef __TEMPERATURE__
#define __TEMPERATURE__
#include <Arduino.h>
#include "Device.h"

class Temperature : public Device {
public:
    Temperature(int pin);
    void initDevice();
    int getTemperature();
};

#endif
