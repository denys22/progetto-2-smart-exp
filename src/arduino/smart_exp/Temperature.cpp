#include "Temperature.h"
#include <Arduino.h>

Temperature::Temperature(int pin) : Device(pin){}

void Temperature::initDevice(){
  pinMode(getPin(),INPUT);
}

int Temperature::getTemperature(){
    float temp = ((analogRead(getPin()) * 0.00488) - 0.5) / 0.01;
    return (int)temp;
}
