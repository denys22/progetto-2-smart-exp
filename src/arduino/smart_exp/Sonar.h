#ifndef __SONAR__
#define __SONAR__
#include <Arduino.h>
#include "Device.h"
#include "Temperature.h"

class Sonar : public Device {

public:
    Sonar(int echo, int trig, int pinTem);
    void initDevice();
    float getDistance();
    void reset();
private:
	int echo;
	int trig;
  float temp;
  Temperature* t;
};

#endif
