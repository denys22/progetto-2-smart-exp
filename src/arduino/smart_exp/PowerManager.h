#ifndef __POWERMANAGER__
#define __POWERMANAGER__

#include "Task.h"
#include "Globals.h"
#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/power.h>
#define SLEEP_TIME 5000

class PowerManager : public TimeTriggeredTask{
public:
    PowerManager(int period);
    void init();
    void tick();
    bool isReady(int schedulerPeriod);

private:
    unsigned long lastTimeTriggeredPir = 0;
    bool needToSleep = false;
};

#endif
