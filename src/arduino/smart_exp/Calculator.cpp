#include "Calculator.h"
#include <stdlib.h>
#include <Arduino.h>

float currentVelocity; 
float currentAcceleration;
bool measurationAreReady;

Calculator::Calculator(){
    reset();
}

float Calculator::calculateVelocity(float newDistance){
    if(lastTime == 0){
      lastTime = millis();
      lastDistance = newDistance;
      return 0;
    }
    unsigned long timeElapsed = millis() - lastTime;
    lastTime = millis();
    if(lastDistance >= 0){
        float lastVelocity = currentVelocity;
        currentVelocity = abs(lastDistance - newDistance)/timeElapsed*1000;
        currentAcceleration = abs(lastVelocity - currentVelocity)/timeElapsed*1000;
        measurationAreReady = true;
    }
    lastDistance = newDistance;
    return currentVelocity;
}

bool Calculator::isObjectPresent(float newDistance){
  if(newDistance > maxDistance){
    countAbsenceOfObject++;
    if(countAbsenceOfObject >= maxAbsenceOfObject){
      state = ABORT_EXPERIMENT;
      reset();
    }
    return false;
  }
  countAbsenceOfObject = 0;
  return true;
}

void Calculator::reset(){
    countAbsenceOfObject = 0;
    lastDistance = -1;
    currentVelocity = -1;
    measurationAreReady = false;
    lastTime = 0;
}
