#ifndef __LED__
#define __LED__ 

#include  <Arduino.h>
#include "Device.h"

class Led : public Device{
public:
    /**
     * @param pin Number of pin than is used by led
     */
    Led(int pin);

    void initDevice();

    void switchOn(); // tunr on Led
    void switchOff(); // Turn off Led
};

#endif
