#include "Led.h"

Led::Led(int pin) : Device(pin){}

void Led::initDevice(){
    pinMode(getPin(), OUTPUT);
}

void Led::switchOff(){
    digitalWrite(getPin(), LOW);
}

void Led::switchOn(){
    digitalWrite(getPin(), HIGH);
}
