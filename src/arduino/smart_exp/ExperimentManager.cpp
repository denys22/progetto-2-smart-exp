#include "ExperimentManager.h"

void sleep();

State state = START;

ExperimentManager::ExperimentManager(int pinPot, int pinTem, int pinSonEcho, int pinSonTrig, int pinSer, int period)
: TimeTriggeredTask(period){
    this->pot = new Potentiometer(pinPot);
    this->son = new Sonar(pinSonEcho, pinSonTrig, pinTem);
    this->ser = new ServoTimer2(pinSer);
    this->cal = new Calculator();
}

ExperimentManager::~ExperimentManager(){
  delete pot;
  delete son;
  delete ser;
  delete cal;
}

bool ExperimentManager::isReady(int schedulerPeriod){
      return updateAndCheckTime(schedulerPeriod) || state != lastState;
}

void ExperimentManager::init(){
    pot->initDevice();
    son->initDevice();
    ser->attach();
}

void ExperimentManager::tick(){
  if(state != lastState){
    lastState = state;
    switch(state){
    case EXPERIMENT:
      if(son->getDistance() > 2){
        state = ABORT_EXPERIMENT;
        timeStart = millis();
      } else {
        cal -> reset();
        myPeriod = pot -> getPeriod();
        timeSonar = millis();
        timeStart = millis();
      }
      break;
    case FINISH_EXPERIMENT:
      ser -> write(0);
      break;
    case ABORT_EXPERIMENT:
      ser -> write(0);
      timeStart = millis();
      break;
    }
  }
  
  switch(state){
    case EXPERIMENT:
      if(millis() - timeStart > MAX_TIME){
        state = FINISH_EXPERIMENT;
      } else if(millis() - timeSonar > myPeriod){
        timeSonar = millis();
        float currDistance = son -> getDistance();
        if(cal -> isObjectPresent(currDistance)){
          ser -> write(cal -> calculateVelocity(currDistance));
        }
      }
      break;
    case ABORT_EXPERIMENT:
      if(millis() - timeStart > ERROR_TIME){
        state = START;
      }
      break;
  }
}
