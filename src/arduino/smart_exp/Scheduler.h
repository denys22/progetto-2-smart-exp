#ifndef __SCHEDULER__
#define __SCHEDULER__
#define __ATMEGA__

#define MAX_TASKS 10

#include "Task.h"
#include <map.h>

class Scheduler {
  
  int basePeriod;
  ustd::map<String, Task*> tasks = ustd::map<String, Task*>(1, MAX_TASKS, 1);

public:
  void init(int basePeriod);  
  virtual bool addTask(Task* task, String name);
  virtual void schedule();
};

#endif
