//progetto realizzato da Filippo Benvenuti, Denys Grushchak e Emanuele Lamagna

#include "Scheduler.h"
#include "ExperimentManager.h"
#include "ButtonsCheck.h"
#include "LedManager.h"
#include "SerialCommunicator.h"
#include "PowerManager.h"

#define LED1_PIN 10 
#define LED2_PIN 11
#define BLINK_PERIOD 200

#define POT_PIN A1
#define THERMOMETER_PIN A0
#define SONAR_ECHO_PIN 9
#define SONAR_TRIG_PIN 8
#define SERVO_PIN 6
#define MIN_SCHED_TIME 20

Scheduler sched;

void setup(){
  Serial.begin(9600);
  sched.init(MIN_SCHED_TIME);
  
  Task* t1 = new ExperimentManager(POT_PIN, THERMOMETER_PIN, SONAR_ECHO_PIN, SONAR_TRIG_PIN, SERVO_PIN, MIN_SCHED_TIME);
  t1->init();

  Task* t2 = new ButtonsCheck();
  t2->init();

  Task* t3 = new LedManager(LED1_PIN, LED2_PIN, BLINK_PERIOD);
  t3->init();

  Task* t4 = new SerialCommunicator();
  t4 -> init();

  Task* t5 = new PowerManager(1000);
  t5 -> init();

  sched.addTask(t1, "ExperimentManager");
  sched.addTask(t2, "ButtonCheck");
  sched.addTask(t3, "LedManager");
  sched.addTask(t4, "SerialCommunicator");
  sched.addTask(t5, "PowerManager");

  Serial.println("Calibrazione sensori...");
  delay(2000);
  Serial.println("Pronto!");

}

void loop(){
  sched.schedule();
}
