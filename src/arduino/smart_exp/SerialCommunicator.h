#ifndef __SERIALCOMMUNICATOR__
#define __SERIALCOMMUNICATOR__

#include <Arduino.h>
#include "Task.h"
#include "Globals.h"

class SerialCommunicator : public Task{
  void init();
  void tick();
  bool isReady(int shedulerPeriod);
};

#endif
