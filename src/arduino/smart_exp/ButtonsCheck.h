#ifndef __BUTTONSCHECK__
#define __BUTTONSCHECK__

#include "Task.h"
#include "Globals.h"
#include <Arduino.h>

class ButtonsCheck: public Task{
  public:
    void tick();
    bool isReady(int period);
    void init();
};

#endif
