#include "Potentiometer.h"
#include <math.h>

int Potentiometer::getPeriod(){
  return (1000/(map(analogRead(getPin()), 0, 1023, MIN_FREQ, MAX_FREQ)))/20*20;
}

void Potentiometer::initDevice(){
  pinMode(getPin(),INPUT);
}
