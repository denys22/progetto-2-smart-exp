#ifndef __POTENTIOMETER__
#define __POTENTIOMETER__

#include <Arduino.h>
#include "Device.h"

#define MIN_FREQ 1
#define MAX_FREQ 50

class Potentiometer : public Device {

public:
    Potentiometer(int pin) : Device(pin){};
    void initDevice();
    int getPeriod();

};

#endif
