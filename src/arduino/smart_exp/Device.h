#ifndef __SENSOR__
#define  __SENSOR__ 

#include <Arduino.h>

class Device{
public:
    /*
     * @param pin that is used by device
     */
    Device(int pin){
        this->pin = pin;
    }

    int getPin(){
      return this->pin;
    }

    virtual void initDevice(); // Initialize hardware of device

private:
    int pin;
};

#endif
