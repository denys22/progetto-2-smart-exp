#include <Arduino.h>
#include "Sonar.h"


Sonar::Sonar(int echo, int trig, int pinTem) : Device(echo){
      this->echo = echo;
      this->trig = trig;
      t = new Temperature(pinTem);
}

float Sonar::getDistance(){
    digitalWrite(trig,LOW);
    delayMicroseconds(5);
    digitalWrite(trig,HIGH);
    delayMicroseconds(10);
    digitalWrite(trig,LOW);

	const float vs = 331.45 + 0.62*/*temp*/20;		// Velocità del suono, 20 è la temperatura.
	float tUS = pulseIn(echo, HIGH);		// Restituisce i microsecondi passati dalla richiesta alla risposta del sonar.
    //Serial.println(String(echo) + String(trig));
    float ts = tUS / 1000.0 / 1000.0 / 2;	// Trasformiamo in secondi e dimezziamo per considerare solo una volta la distanza.
    return ts*vs;	
}

void Sonar::reset(){
  temp = t -> getTemperature();
}

void Sonar::initDevice(){
  pinMode(echo, INPUT);
  pinMode(trig, OUTPUT);
  t -> initDevice();
//delay(10000);
  //Serial.println("Sonar pronto");
}
