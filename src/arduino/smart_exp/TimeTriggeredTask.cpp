#ifndef __TIMETRIGGEREDTASK__
#define __TIMETRIGGEREDTASK__

#include "Task.h"

TimeTriggeredTask::TimeTriggeredTask(int period){
  myPeriod = period; 
  timeElapsed = 0;
}

bool TimeTriggeredTask::isReady(int schedulerPeriod){
  return updateAndCheckTime(schedulerPeriod);
}

bool TimeTriggeredTask::updateAndCheckTime(int basePeriod){
  timeElapsed += basePeriod;
  if (timeElapsed >= myPeriod){
    timeElapsed = 0;
    return true;
  } 
  return false; 
}

#endif
