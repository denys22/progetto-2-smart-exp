#include "PowerManager.h"

#define EI_ARDUINO_INTERRUPTED_PIN
#define LIBCALL_ENABLEINTERRUPT
#include <EnableInterrupt.h>

volatile bool isPirTriggered = false;

void sleep();
void pirHandler();

PowerManager::PowerManager(int period) : TimeTriggeredTask(period){}

void PowerManager::init(){
    enableInterrupt(4, pirHandler, RISING);
}

void PowerManager::tick(){
    if(state != lastState){
       lastState = state;
       if(state == START){
        lastTimeTriggeredPir = millis();
       }
    }
    if(isPirTriggered){
      isPirTriggered = false;
      lastTimeTriggeredPir = millis();
    }
    if(needToSleep){
      needToSleep = false;
      delay(1000);
      sleep();
      state = START;
      lastTimeTriggeredPir = millis();
    }
    if(millis() - lastTimeTriggeredPir > SLEEP_TIME && state == START){
      needToSleep = true;
      state = POWER_SAVE;
    }
}

bool PowerManager::isReady(int schedulerPeriod){
    return (updateAndCheckTime(schedulerPeriod) || isPirTriggered);
}

void pirHandler(){
  unsigned long ts = millis();
  static unsigned long prevts;
  if(ts-prevts > 250){  //per il bouncing
    isPirTriggered = true; 
    prevts = ts;
  }
}

void sleep(){
 set_sleep_mode(SLEEP_MODE_PWR_DOWN);
 sleep_enable();
 power_adc_disable();
 power_spi_disable();
 power_timer0_disable();
 power_timer2_disable();
 power_twi_disable();
 sleep_mode();
 sleep_disable();
 power_all_enable();
}
