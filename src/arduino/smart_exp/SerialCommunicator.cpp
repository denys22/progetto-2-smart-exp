#include "SerialCommunicator.h"

void SerialCommunicator::init(){
  Serial.begin(9600);
  lastState = FINISH_EXPERIMENT;
}

void SerialCommunicator::tick(){
  if(state != lastState){
    lastState = state;
    Serial.println("1");
    Serial.println(state);
  }
  if(state == EXPERIMENT && measurationAreReady && currentVelocity>=0 && currentAcceleration>=0){
    Serial.println("2");
    Serial.println(currentVelocity);
    Serial.println(currentAcceleration);
    measurationAreReady = false;
  }else if(state == FINISH_EXPERIMENT && Serial.available() > 0){
    String content = Serial.readStringUntil('\n');
    if (content == "ok"){
      state = START;
      Serial.println("Correct restart of system");
      Serial.println("1");
      Serial.println(state);
    }else{
      Serial.println("Error, \"ok\" is non recognized");
    }
  }else if(Serial.available() > 0){ //clear out the serial buffer
    while (Serial.available() > 0) {
      char t =  Serial.read();
    }
  }
}

bool SerialCommunicator::isReady(int schedulerPeriod){
  return (measurationAreReady || state != lastState || state == FINISH_EXPERIMENT);
}
