#ifndef __GLOBALS__
#define __GLOBALS__

enum State {START, POWER_SAVE, EXPERIMENT, ABORT_EXPERIMENT, FINISH_EXPERIMENT};

extern State state;

volatile extern bool startButtonPressed;
volatile extern bool stopButtonPressed;

volatile extern bool isPirTriggered;

extern bool measurationAreReady;

/* before read the variables check if value is >= 0 */

extern float currentVelocity; 
extern float currentAcceleration;


#endif
