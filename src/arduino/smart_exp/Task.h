#ifndef __TASK__
#define __TASK__

#include "Globals.h"

class Task {
public:
  virtual void init() = 0; // Inizialize decices of task 
  virtual void tick() = 0; // run main work of task
  virtual bool isReady(int schedulerPeriod) = 0; // controls whether the task should be started. Return TRUE if task must me execute

protected:
  State lastState = START; // The last state that was incontred by task
};


class TimeTriggeredTask : public Task{
public:
  virtual void init() = 0; // method of class Task
  virtual void tick() = 0; // method of class Task
  
  TimeTriggeredTask(int period); // method of class Task
  bool isReady(int schedulerPeriod); // method of class Task
  
protected:
  /*
   * Check whether time of execution is arrive and update time counter
   * 
   * @return true if the task must be executed
   */
  bool updateAndCheckTime(int basePeriod); 
  int myPeriod; // period of execution of the task
  
private:
  int timeElapsed; // time elapsed by last execution
};

#endif
