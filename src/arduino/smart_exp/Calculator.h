#ifndef __CALCULATOR__
#define __CALCULATOR__

#include "Globals.h"

class Calculator{
public:
    Calculator();
    /**
     * Takes two distance to an object and calculates the velocity and acceleration of it.
     * The velocity and acceleration are stored in the global variables.
     * 
     * @param newDistance new distance of object
     * @return a negative value if there are not enough dates for calculate velocity.
     * @return a velocity of object.
     */ 
    float calculateVelocity(float newDistance);

    /**
     * Controls if an object is nearer than maxDistance, if it isn't than object is consider absent.
     * If the object is absent more than maxAbsenceOfObject times in row, than the state of system will be setted to ABORT_EXPERIMENT
     * 
     * @return true if the object is nearer than maxDistance, otherwise return false and increment the countAbsenceOfObject variable
     */
    bool isObjectPresent(float newDistance);
    
    void reset(); // reset the calculator. Call this method if you want to start a new experiment

private:
    const int maxAbsenceOfObject = 3;
    const int maxDistance = 2;
    float lastDistance;
    int countAbsenceOfObject;
    unsigned long lastTime = 0;
};

#endif // !__CALCULATOR__
