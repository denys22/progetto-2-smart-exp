#include "Scheduler.h"
#include <TimerOne.h>

volatile bool timerFlag;

void timerHandler(void){
  timerFlag = true;
}

void Scheduler::init(int basePeriod){
  this->basePeriod = basePeriod;
  timerFlag = false;
  long period = 1000l*basePeriod;
  Timer1.initialize(period);
  Timer1.attachInterrupt(timerHandler);
}

bool Scheduler::addTask(Task* task, String name){
  if(tasks.length() < MAX_TASKS && tasks.find(name) == -1){
    tasks[name] = task;
    return true;
  }
  return false;
}

void Scheduler::schedule(){
  while (!timerFlag){}
  timerFlag = false;
  for (int i = 0; i < tasks.length(); i++){
    if (tasks.values[i]->isReady(basePeriod)){
      tasks.values[i]->tick();
    }
  }
}
