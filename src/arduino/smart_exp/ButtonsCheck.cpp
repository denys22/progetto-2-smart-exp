#include "ButtonsCheck.h"

#define EI_ARDUINO_INTERRUPTED_PIN
#include <EnableInterrupt.h>

volatile unsigned long prevts;

volatile bool startButtonPressed;
volatile bool stopButtonPressed;

void startHandler();
void stopHandler();

void ButtonsCheck::tick(){
  if(startButtonPressed){
    startButtonPressed = false;
    if(state == START){
      state = EXPERIMENT;
    }
  }
  if(stopButtonPressed){
    stopButtonPressed = false;
    if(state == EXPERIMENT){
      state = FINISH_EXPERIMENT;
    }
  }
}

void ButtonsCheck::init(){
  enableInterrupt(2, startHandler, RISING);
  enableInterrupt(3, stopHandler, RISING);
}

bool ButtonsCheck::isReady(int period){
  return startButtonPressed || stopButtonPressed;
}

void startHandler(){
  unsigned long ts = millis();
  if(ts-prevts > 250){  //per il bouncing
    startButtonPressed = true; 
    prevts = ts;
  }
}

void stopHandler(){
  unsigned long ts = millis();
  if(ts-prevts > 250){  //per il bouncing
    stopButtonPressed = true;
    prevts = ts;
  }
}
