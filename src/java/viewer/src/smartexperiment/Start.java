package smartexperiment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.SwingUtilities;

public class Start {
	final static String STATE = "1";
	final static String MESURATION = "2";
	final static Random ran = new Random();
	

	public static void main(String[] args) throws Exception {
		GUI gui = new GUI();
		gui.getFrame().setVisible(true);
		
		CommChannel channel = new SerialCommChannel("COM3",9600);	
		gui.addActionListenerToOkButton(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				channel.sendMsg("ok");
			}
		});

		/* attesa necessaria per fare in modo che Arduino completi il reboot */
		gui.addText("Waiting Arduino for rebooting...");		
		Thread.sleep(4000);
		gui.addText("Ready.");	
		
		String msg;
		while (true){
			msg = channel.receiveMsg();
			System.out.println("Received: " + msg);
			if(msg.equals(STATE)) {
				msg = channel.receiveMsg();
				gui.setState(msg);
			}else if(msg.equals(MESURATION)) {
				SwingUtilities.invokeAndWait(new Runnable() {
					
					@Override
					public void run() {
						try {
							gui.updateVelocity(Float.valueOf(channel.receiveMsg()));
						} catch (NumberFormatException | InterruptedException e1) {
							e1.printStackTrace();
						}
						try {
							gui.updateAcceleration(Float.valueOf(channel.receiveMsg()));
						} catch (NumberFormatException | InterruptedException e) {
							e.printStackTrace();
						}
						
					}
				});
				
			}else {
				gui.addText(msg);
			}			
		}
	}
}
