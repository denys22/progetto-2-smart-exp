package smartexperiment;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import javax.swing.JButton;

import java.awt.Color;

import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.SwingConstants;


public class GUI {

	private JFrame frame;
	private JTextArea textPane;
	private DynamicTimeSeriesChart chartAcc = new DynamicTimeSeriesChart("Acceleration");
	private DynamicTimeSeriesChart chartVel = new DynamicTimeSeriesChart("Velocity");
	private JLabel label;
	private JButton okButton;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	public void addText(String s) {
		this.textPane.append(s + "\n");
	}
	
	public void  updateVelocity(float f) {
		this.chartVel.update(f);
	}
	
	public void  updateAcceleration(float f) {
		this.chartAcc.update(f);
	}
	
	public JFrame getFrame() {
		return this.frame;
	}
	
	public void setState(String state) {
		String s = "";
		if(state.equals("0")) {
			s = "start";
		}else if(state.equals("1")) {
			s = "sleep mode";
		}else if(state.equals("2")) {
			s = "experiment";
		}else if(state.equals("3")) {
			s = "abort experiment";
		}else if(state.equals("4")) {
			s = "finish experiment";
		}else {
			s = "unknown";
		}
		this.label.setText("System state: " + s);
		addText("New state: " + s);
	}
	
	public void addActionListenerToOkButton(ActionListener a) {
		this.okButton.addActionListener(a);
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1330, 587);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		
		label = new JLabel("System state: ");
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setFont(new Font("Cambria", Font.BOLD, 15));
		label.setBackground(new Color(153, 153, 255));
		
		JPanel mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(250, 500));
		mainPanel.setLayout(new BorderLayout(0, 0));
		
		
		this.textPane = new JTextArea();
		textPane.setBackground(new Color(153, 204, 204));
		textPane.setEditable(false);
		frame.getContentPane().add(mainPanel, BorderLayout.EAST);
		
		JScrollPane scrollPane = new JScrollPane(textPane);
		scrollPane.setAutoscrolls(true);
		scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				e.getAdjustable().setValue(e.getAdjustable().getMaximum());
				
			}
		});
		mainPanel.add(label, BorderLayout.NORTH);
		mainPanel.add(scrollPane, BorderLayout.CENTER);
		
		okButton = new JButton("OK");
		mainPanel.add(okButton, BorderLayout.SOUTH);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(new Color(102, 153, 153));
		frame.getContentPane().add(centerPanel, BorderLayout.CENTER);
		
		JPanel accPanel = new JPanel();
		centerPanel.add(accPanel);
		accPanel.setBackground(new Color(102, 153, 153));
		
		JPanel velPanel_1 = new JPanel();
		velPanel_1.setBackground(new Color(102, 153, 153));
		centerPanel.add(velPanel_1);
		
		velPanel_1.add(chartVel);
		accPanel.add(chartAcc);
	}

}
